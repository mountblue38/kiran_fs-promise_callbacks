const fs = require("fs");
const path = require("path");

function problem1() {

    return new Promise((resolve, reject) => {

        fs.mkdir("./JSONfiles", { recursive: true }, (err) => {

            if (err) {
                console.error("Error in the Making Directory ");
                reject(err);
            } else {
                resolve();
            }
        });
    })
        .then(() => {
            let randomNumber = Math.floor(Math.random() * 10) + 1;
            console.log(`Creating ${randomNumber} files`);

            let randomArr = Array(randomNumber)
                .fill(0)
                .map((_, index) => {
                    return `./JSONfiles/randomfile${index}.json`;
                });
            Promise.all(

                randomArr.map((filePath) => {

                    return new Promise((resolve, reject) => {
                        let randomFilePath = path.join(__dirname, filePath);

                        fs.writeFile(randomFilePath, JSON.stringify("Hello"), (err) => {
                            if (err) {
                                reject(err);
                            } else {
                                console.log(`Done creating the file ${path.basename(randomFilePath)}`);
                                resolve();
                            }
                        });
                    });
                })
            )
                .then(() => {
                    Promise.all(
                        randomArr.map((filePath) => {

                            return new Promise((resolve, reject) => {

                                fs.unlink(filePath, (err) => {
                                    if (err) {
                                        reject(err);
                                    } else {
                                        console.log(`Done deleting file ${path.basename(filePath)}`);
                                        resolve();
                                    }
                                });
                            });
                        })              // catch err from the promise all of deleting the file
                    ).catch((err) => {      
                        console.error(err);
                    });
                })              // catch err from the promise all of creating the file
                .catch((err) => {
                    err;
                });
        })      // catch err from the making directory the file
        .catch((err) => {
            console.log(err);
        });
}

module.exports = problem1;
