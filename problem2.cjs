const fs = require("fs");
const path = require("path");

function problem2() {
    const nameStorerPath = path.join(__dirname, "./filesName.txt");
    const textfilepath = path.join(__dirname, "./lipsum.txt");

    return new Promise((resolve, reject) => {
        fs.readFile(textfilepath, "utf-8", (err, data) => {     
            if (err) {
                console.error("Error in the start ");
                reject(err);
            } else {
                console.log("started reading the file ....");
                resolve(data);
            }
        });
    })
        .then((data) => {
            return new Promise((resolve, reject) => {
                const filePath = path.join(__dirname, "./uppercase.txt");
                fs.writeFile(filePath, data.toUpperCase(), (err) => {
                    if (err) {
                        console.error("Error in the Writing a uppercase file");
                        reject(err);
                    } else {
                        console.log("started writing uppercase.txt the file");
                        resolve(filePath);
                    }
                });
            });
        })
        .then((filePath) => {
            return new Promise((resolve, reject) => {
                const fileName = path.basename(filePath);
                fs.writeFile(nameStorerPath, fileName + "\n", (err) => {
                    if (err) {
                        console.error("Error in Appending a file");
                        reject(err);
                    } else {
                        console.log(`${fileName} (filename) is pushed to filenames.txt`);
                        resolve(filePath);
                    }
                });
            });
        })
        .then((filePath) => {
            return new Promise((resolve, reject) => {
                fs.readFile(filePath, "utf-8", (err, data) => {
                    if (err) {
                        console.error("Error in Reading the uppercase file");
                        reject(err);
                    } else {
                        console.log(`started reading the uppercase.txt file....`);
                        resolve(data);
                    }
                });
            });
        })
        .then((data) => {
            return new Promise((resolve, reject) => {
                const filePath = path.join(__dirname, "./sentence.txt");
                let lowercaseData = data.toLowerCase();

                let arrOfSentence = lowercaseData.split(". ")
                    .map((sentence) => {
                        return sentence.charAt(0).toUpperCase() + sentence.slice(1);
                    })
                    .join(".\n");

                fs.writeFile(filePath, String(arrOfSentence), (err) => {
                    if (err) {
                        console.error("Error in writing the sentence file");
                        reject(err);
                    } else {
                        console.log(`started writing the ${path.basename(filePath)} file....`);
                        resolve(filePath);
                    }
                });
            });
        })
        .then((filePath) => {
            return new Promise((resolve, reject) => {
                const fileName = path.basename(filePath);

                fs.appendFile(nameStorerPath, fileName + "\n", (err) => {
                    if (err) {
                        console.error("Error in the appending the file name");
                        reject(err);
                    } else {
                        console.log(`${fileName} (filename) is pushed to filenames.txt`);
                        resolve(nameStorerPath);
                    }
                });
            });
        })
        .then((nameStorerPath) => {
            return new Promise((resolve, reject) => {
                fs.readFile(nameStorerPath, "utf-8", (err, data) => {
                    if (err) {
                        console.error("Error in the Reading the file");
                        reject(err);
                    } else {
                        console.log("Started reading and splitting filnenames in the fileNames.txt");
                        let arrFilename = data.split("\n").slice(0, -1);
                        resolve(arrFilename);
                    }
                });
            });
        })
        .then((arrFilename) => {
            Promise.all(
            arrFilename.map((fileName) => {
                return new Promise((resolve, reject) => {
                    fs.unlink(`./${fileName}`, (err) => {
                        if (err) {
                            console.error("Error in the delting the file");
                            reject(err);
                        } else {
                            console.log(`Done deleting the file ${fileName}`);
                            resolve("completed");
                        }
                    });
                });
            })
            ).catch((err)=>{
                console.log(err);
            })
        })
        .catch((err) => {
            console.log(err);
        });
}

module.exports = problem2;
